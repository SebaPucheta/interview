module.exports = {
  "extends": [
    "./node_modules/eslint-config-airbnb-base/legacy.js"
  ],
  "rules": {
    "class-methods-use-this": "off",
    "linebreak-style": 0,
    "eol-last": 0,
    "no-underscore-dangle": 0
  },
  "env": {
    "es6": true
  },
  "parserOptions": {
    "ecmaVersion": 2017
  }
};
