const jwt = require('jsonwebtoken');
const config = require('config');
const errorsHandler = require('../common/errors-handler');

exports.isLogged = (req, res, next) => {
  try {
    if (jwt.verify(req.headers.authorization.split(' ')[1], config.get('secretKey'))) {
      next();
    } else {
      res.status(errorsHandler.getStatus('UNAUTHORIZED'));
      res.send(errorsHandler.getSend('UNAUTHORIZED'));
    }
  } catch (error) {
    res.status(errorsHandler.getStatus('UNAUTHORIZED'));
    res.send(errorsHandler.getSend('UNAUTHORIZED'));
  }
};
