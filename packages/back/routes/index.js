const express = require('express');
const auth = require('./auth.route');
const game = require('./game.route');

const router = express();

router.use('/auth', auth);
router.use('/games', game);

module.exports = router;
