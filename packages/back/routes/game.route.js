const router = require('express').Router();
const gameController = require('../controllers/game.controller');
const middleware = require('../middlewares/is-user-logged.middleware');

router.post('', middleware.isLogged, gameController.save);
router.delete('/:id', middleware.isLogged, gameController.delete);
router.get('/:id', middleware.isLogged, gameController.findById);
router.get('', middleware.isLogged, gameController.find);
router.put('/:id', middleware.isLogged, gameController.update);

module.exports = router;
