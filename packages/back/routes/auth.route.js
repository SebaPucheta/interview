const router = require('express').Router();
const authController = require('../controllers/auth.controller');
const middleware = require('../middlewares/is-user-logged.middleware');

router.post('/sign-in', authController.signIn);
router.post('/sign-up', authController.signUp);
router.get('/test', middleware.isLogged);

module.exports = router;
