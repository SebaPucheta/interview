const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  password: String
});

const UserModel = mongoose.model('User', UserSchema);

class User {
  async save(user) {
    const newUser = new UserModel(user);
    await newUser.save(user);
  }

  async find(email) {
    const user = await UserModel.findOne({ email });
    return user;
  }
}

module.exports = User;
