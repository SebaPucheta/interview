const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema({
  startTime: String,
  endTime: String,
  turnsUsed: Number,
  status: String,
  matrix: Array,
  turnsMaxCount: Number
});

const GameModel = mongoose.model('Game', GameSchema);

class Game {
  async save(game) {
    let newGame = new GameModel(game);
    newGame = await newGame.save(game);
    return newGame;
  }

  async findById(_id) {
    const game = await GameModel.findOne({ _id });
    return game;
  }

  async find() {
    const games = await GameModel.find();
    return games;
  }

  async delete(_id) {
    const games = await GameModel.deleteOne({ _id });
    return games;
  }

  async update(_id, game) {
    const updatedGame = await GameModel.updateOne({ _id }, game);
    return updatedGame;
  }
}

module.exports = Game;
