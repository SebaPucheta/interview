const config = require('config');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const express = require('express');
const cors = require('cors');
const corsOptions = {
  origin: config.get('cors.origins'),
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};
const winston = require('./services/winston');

const app = express();
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json());

app.use(cors(corsOptions));

const router = require('./routes');

app.use('/api', router);
const port = config.get('port');

mongoose.connect('mongodb://SebaPucheta:Seba123456@ds217548.mlab.com:17548/react-graphql-apollo', { useNewUrlParser: true });

app.listen(port, () => {
  winston.info(`It's listening for port ${port}`);
});
