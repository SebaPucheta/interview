const jwt = require('jsonwebtoken');
const config = require('config');
const UserModel = require('../models/user.model');
const userModel = new UserModel();
const bcrypt = require('bcrypt');
const errorsHandler = require('../common/errors-handler');

const getToken = (email, password) => jwt.sign({ email, password }, config.get('secretKey'));

const createHash = async password => {
  const newHash = await bcrypt.hash(password, config.get('saltRounds'));
  return newHash;
};

const compareHash = async (password, passwordHash) => {
  const result = await bcrypt.compare(password, passwordHash);
  return result;
};

exports.signIn = async (req, res) => {
  try {
    const user = await userModel.find(req.body.email, req.body.password);
    if (user && await compareHash(req.body.password, user.password)) {
      res.status(errorsHandler.getStatus('OK'));
      res.send({
        token: getToken(user.email, user.password),
        user: { email: user.email, name: user.name }
      });
    } else {
      res.status(errorsHandler.getStatus('UNAUTHORIZED'));
      res.send(errorsHandler.getSend('UNAUTHORIZED'));
    }
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};

exports.signUp = async (req, res) => {
  try {
    const password = await createHash(req.body.password);
    const user = {
      email: req.body.email,
      password,
      name: req.body.password
    };

    await userModel.save(user);
    res.send({
      token: getToken(user.email, user.password),
      user: { email: user.email, name: user.name }
    });
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};
