const GameModel = require('../models/game.model');
const gameModel = new GameModel();
const errorsHandler = require('../common/errors-handler');

exports.save = async (req, res) => {
  try {
    let game = {
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      turnsUsed: req.body.turnsUsed,
      status: req.body.status,
      matrix: req.body.matrix,
      turnsMaxCount: req.body.turnsMaxCount
    };

    game = await gameModel.save(game);
    res.status(errorsHandler.getStatus('OK'));
    res.send(game);
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};

exports.update = async (req, res) => {
  try {
    let game = {
      startTime: req.body.startTime,
      endTime: req.body.endTime,
      turnsUsed: req.body.turnsUsed,
      status: req.body.status,
      matrix: req.body.matrix,
      turnsMaxCount: req.body.turnsMaxCount
    };
    await gameModel.update(req.params.id, game);
    game._id = req.params.id;
    res.status(errorsHandler.getStatus('OK'));
    res.send(game);
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};

exports.delete = async (req, res) => {
  try {
    const games = await gameModel.delete(req.params.id);
    res.status(errorsHandler.getStatus('OK'));
    res.send(games);
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};

exports.findById = async (req, res) => {
  try {
    const game = await gameModel.findById(req.params.id);
    res.status(errorsHandler.getStatus('OK'));
    res.send(game);
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};

exports.find = async (req, res) => {
  try {
    const games = await gameModel.find();
    res.status(errorsHandler.getStatus('OK'));
    res.send(games);
  } catch (error) {
    res.status(errorsHandler.getStatus('INTERNAL_SERVER_ERROR'));
    res.send(errorsHandler.getSend('INTERNAL_SERVER_ERROR'));
  }
};
