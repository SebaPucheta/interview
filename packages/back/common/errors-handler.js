const HttpStatus = require('http-status-codes');

exports.getSend = (code) => {
  return {
    code: HttpStatus[code],
    message: HttpStatus.getStatusText(HttpStatus[code])
  };
};

exports.getStatus = (code) => {
  return HttpStatus[code];
};
