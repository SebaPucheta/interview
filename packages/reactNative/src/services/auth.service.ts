const url = 'http://192.168.0.14:3000/api';

export default class AuthService {
  private authUrl = `${url}/auth`;

  public async login(email, password) {
    try {
      const body = { email, password };
      const response = await fetch(
        `${this.authUrl}/sign-in`,
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(body) // body data type must match "Content-Type" header
        }
      );
      return await response.json();
    } catch (err) {
      console.log('err =>', err)
    }
  }

  public async signUp(email, password) {
    try {
      const body = { email, password };
      const response = await fetch(
        `${this.authUrl}/sign-up`,
        {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(body) // body data type must match "Content-Type" header
        }
      );
      return await response.json();
    } catch (err) {
      console.log('err =>', err)
    }
  }
}
