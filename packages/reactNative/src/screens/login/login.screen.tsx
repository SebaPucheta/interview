import React, { Component } from 'react';
import LoginForm from './login.form';
import LoaderComponent from '../../components/loader/loader.component';
import AuthService from '../../services/auth.service';

export default class LoginScreen extends Component {
  state = {
    email: 'seba.pucheta.17@gmail.com',
    password: '1234',
    error: null,
    sending: false
  };
  private authService = new AuthService();

  onChangeEmail = email => this.setState({email});

  onChangePassword = password => this.setState({password});

  onSubmit = async (props) => {
    this.setState({ sending: true });
    const { email, password } = this.state;
    const response = await this.authService.login(email, password);
    this.setState({ sending: false });
    if (response) {
      props.navigation.navigate('Home');
    }
  }

  getScreen(isLoading: boolean) {
    if (isLoading) {
      return (
        <LoaderComponent></LoaderComponent>
      );
    } else {
      return (
        <LoginForm
          formData = { this.state }
          onSubmit = { () => this.onSubmit(this.props) }
          onChangePassword = { this.onChangePassword }
          onChangeEmail = { this.onChangeEmail }
          onGoToSignUp = {() => this.props.navigation.navigate('SignUp')} />
      );
    }
  }

  render() {
    return this.getScreen(this.state.sending);
  }
}
