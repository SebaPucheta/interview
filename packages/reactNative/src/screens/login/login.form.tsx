import React from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
} from 'react-native';

const LoginForm = ({ onChangeEmail, onChangePassword, onSubmit, onGoToSignUp, formData }) => (
  <View>
    <Text>Login</Text>
    <Text>Email</Text>
    <TextInput
      onChangeText = { onChangeEmail }
      value = { formData.email }
      autoCapitalize = 'none'
    />
    <Text>Password</Text>
    <TextInput
      onChangeText = { onChangePassword }
      value = { formData.password }
      secureTextEntry
      />

    <Button 
      onPress = { onSubmit }
      title = 'Submit'
    />
    <Text
      onPress = { onGoToSignUp }>
        Sign Up
    </Text>
  </View>
)

export default LoginForm;
