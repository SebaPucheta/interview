import React from 'react';
import {
    View,
    Text,
    TextInput,
    Button,
} from 'react-native';

const SignUpForm = ({ onChangeEmail, onChangePassword, onSubmit, formData }) => (
  <View>
    <Text>Sing Up</Text>
    <Text>Email</Text>
    <TextInput
      onChangeText = { onChangeEmail }
      value = { formData.email }
      autoCapitalize = 'none'
    />
    <Text>Password</Text>
    <TextInput
      onChangeText = { onChangePassword }
      value = { formData.password }
      secureTextEntry
      />

    <Button 
      onPress = { onSubmit }
      title = 'Submit'
    />
  </View>
)

export default SignUpForm;
