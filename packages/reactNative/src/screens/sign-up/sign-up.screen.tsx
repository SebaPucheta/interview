import React, { Component } from 'react';
import SignUpForm from './sign-up.form';
import LoaderComponent from '../../components/loader/loader.component';
import AuthService from '../../services/auth.service';

export default class SignUpScreen extends Component {
  state = {
    email: 'seba.pucheta.17@gmail.com',
    password: '1234',
    error: null,
    sending: false
  };
  private authService = new AuthService();

  onChangeEmail = email => this.setState({email});

  onChangePassword = password => this.setState({password});

  onSubmit = async (props) => {
    this.setState({ sending: true });
    const { email, password } = this.state;
    const response = await this.authService.signUp(email, password);
    this.setState({ sending: false });
    if (response) {
      props.navigation.navigate('Home');
    } else {

    }
  }

  getScreen(isLoading: boolean) {
    if (isLoading) {
      return (
        <LoaderComponent></LoaderComponent>
      );
    } else {
      return (
        <SignUpForm
          formData = { this.state }
          onSubmit = { () => this.onSubmit(this.props) }
          onChangePassword = { this.onChangePassword }
          onChangeEmail = { this.onChangeEmail } />
      );
    }
  }

  render() {
    return this.getScreen(this.state.sending);
  }
}
