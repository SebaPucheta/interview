import React from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator
} from 'react-native';

const LoaderComponent = ({ size = 100, hidesWhenStopped = true, color = '#00ff00', animating = undefined }) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center'
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    }
  });

  return (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator 
        size = { size }
        color = { color }
        hidesWhenStopped = { hidesWhenStopped }
        animating = { animating }/>
    </View>
  )
}


export default LoaderComponent;
