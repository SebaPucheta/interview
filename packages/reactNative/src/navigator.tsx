import { createStackNavigator } from 'react-navigation-stack';
import LoginScreen from './screens/login/login.screen';
import SignUpScreen from './screens/sign-up/sign-up.screen';
import HomeScreen from './screens/home/home.screen';

const MainNavigator = createStackNavigator(
  {
    Login: { screen: LoginScreen },
    SignUp: { screen: SignUpScreen },
    Home: { screen: HomeScreen },
  },
  {
    initialRouteName: 'Login',
  }
);

export default MainNavigator;
